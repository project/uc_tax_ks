
This is a module for calculating Kansas Sales tax in Ubercart, in Drupal.

It is recommended to use the latest version of Ubercart 6.x with this
module. Ubercart 6.x-2.0-beta5 and later versions store MUCH more
information about tax calculations in the database, for much better
reporting. You will probably need this information to file your sales tax
reports.


INSTALLATION AND SETUP

a) Install and enable the module in the normal way for Drupal.

b) Visit your Ubercart Store Administration page, Configuration
section, and choose "Kansas sales tax settings". (path:
admin/store/settings/uc_tax_ks)

c) On this page, settings:
   - Default sales tax rate, in case address lookup fails
   - Product types to be taxed
   - Line items to be taxed (such as shipping charges)

OPERATION OF MODULE

Once you install and configure the module, if someone makes an order
and the Delivery Address is a Kansas address, all taxable products
and line items that you configured will be taxed according to the
customer's tax rate. If the order has no shippable items, Ubercart
will not collect a delivery address, so the Billing Address is used.

The rate is determined from the Kansas Department of Revenue's
lookup service.  (See http://www.ksrevenue.org/webservices.htm for more info.)
If the rate lookup fails, the default tax rate you configured will be used
instead.

Once the rate is found, the tax is calculated by multiplying the
product quantity * price * rate for each taxable product in the order,
plus line item price * rate for each taxable line item, and the
Ubercart Tax module handles actually adding the tax line item to the
order.

This module was developed by Jennifer Hodgdon of Poplar ProductivityWare.
http://poplarware.com
Initial development was sponsored by Nuvidia.
http://www.nuvidia.com/
